<?php

class StudentInfo
{
    public $intialize='We are starting...';
    public $std_id="";
    public $std_name="Inman";
    public $std_cgpa=0.00;
    public function __construct($name){
        $this->intialize=$name;
        echo $this->intialize.'<br>';
    }
    public function __destruct()
    {
        echo 'we are finished';
    }

    public function set_id($sl_id){
        $this->std_id = $sl_id;
    }
    public function set_name($sl_name){
        $this->std_name = $sl_name;
    }
    public function set_cgpa($sl_gpa){
        $this->std_cgpa = $sl_gpa;
    }
    public function get_id(){
        return $this->std_id;
    }
    public function get_name(){
        return $this->std_name;
    }
    public function get_cgpa(){
        return $this->std_cgpa;
    }
    public function showDetails(){
        echo "Student Id : " .$this->std_id .'<br>';
        echo "Student name : " .$this->std_name.'<br>';
        echo "Student CGPA : " .$this->std_cgpa.'<br>';
    }

}
$obj = new StudentInfo('Mohammad Ali');
$obj->set_id("seip143229");
$obj->set_name("Ali");
$obj->set_cgpa("3.703");

echo $obj->showDetails();
$obj->set_id("seip143299");
$obj->set_name("Charles");
$obj->set_cgpa("3.993");
echo $obj->showDetails();