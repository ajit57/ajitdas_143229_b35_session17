<?php
include_once('oop_basics1.php');
class SchoolStudent extends StudentInfo{
    public function goSchool(){
        echo 'Going To School';
    }
}
$schoolStudent= new SchoolStudent('Charles Valerio Howlader');
$schoolStudent->set_id("seip143253");
$schoolStudent->set_name("Charles");
$schoolStudent->set_cgpa("4.00");

echo $schoolStudent->showDetails();
